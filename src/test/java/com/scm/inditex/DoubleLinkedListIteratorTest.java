package com.scm.inditex;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by domingo on 27/10/17.
 */
public class DoubleLinkedListIteratorTest {

    private List<String> list = new DoubleLinkedList<>();

    {
        list.addAll(Arrays.asList("Larry", "Moe", "Curly"));
    }

    @Test
    public void hasNext() throws Exception {
        ListIterator<String> iterator = list.listIterator();
        assertThat(iterator.hasNext(), is(true));
        iterator.next();
        assertThat(iterator.hasNext(), is(true));
        iterator.next();
        assertThat(iterator.hasNext(), is(true));
        iterator.next();
        assertThat(iterator.hasNext(), is(false));
        try {
            iterator.next();
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(NoSuchElementException.class)));
        }
    }

    @Test
    public void next() throws Exception {
        ListIterator<String> iterator = list.listIterator();
        assertThat(iterator.next(), is(equalTo("Larry")));
        assertThat(iterator.next(), is(equalTo("Moe")));
        assertThat(iterator.next(), is(equalTo("Curly")));
        try {
            iterator.next();
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(NoSuchElementException.class)));
        }

        //with iterator in a middle node
        iterator = list.listIterator(1);
        assertThat(iterator.next(), is(equalTo("Moe")));
    }

    @Test
    public void hasPrevious() throws Exception {
        ListIterator<String> iterator = list.listIterator();
        assertThat(iterator.hasPrevious(), is(false));
        iterator.next();
        assertThat(iterator.hasPrevious(), is(true));
        iterator.next();
        iterator.next();
        assertThat(iterator.hasPrevious(), is(true));
        try {
            iterator.next();
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(NoSuchElementException.class)));
        }
        assertThat(iterator.hasPrevious(), is(true));
    }

    @Test
    public void previous() throws Exception {
        ListIterator<String> iterator = list.listIterator();
        try {
            iterator.previous();
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(NoSuchElementException.class)));
        }
        assertThat(iterator.next(), is(equalTo("Larry")));
        assertThat(iterator.next(), is(equalTo("Moe")));
        assertThat(iterator.previous(), is(equalTo("Moe")));
        assertThat(iterator.next(), is(equalTo("Moe")));
        assertThat(iterator.previous(), is(equalTo("Moe")));
        assertThat(iterator.next(), is(equalTo("Moe")));
        assertThat(iterator.next(), is(equalTo("Curly")));
        assertThat(iterator.previous(), is(equalTo("Curly")));
        assertThat(iterator.previous(), is(equalTo("Moe")));
        assertThat(iterator.previous(), is(equalTo("Larry")));
        try {
            iterator.previous();
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(NoSuchElementException.class)));
        }

        //with iterator in a middle node
        iterator = list.listIterator(1);
        assertThat(iterator.previous(), is(equalTo("Larry")));
    }

    @Test
    public void nextIndex() throws Exception {
        ListIterator<String> iterator = list.listIterator();
        assertThat(iterator.nextIndex(), is(0));
        assertThat(iterator.next(), is(equalTo("Larry")));
        assertThat(iterator.nextIndex(), is(1));
        assertThat(iterator.next(), is(equalTo("Moe")));
        assertThat(iterator.nextIndex(), is(2));
        assertThat(iterator.next(), is(equalTo("Curly")));
        assertThat(iterator.nextIndex(), is(3));
        try {
            iterator.next();
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(NoSuchElementException.class)));
        }
    }

    @Test
    public void previousIndex() throws Exception {
        ListIterator<String> iterator = list.listIterator();
        assertThat(iterator.previousIndex(), is(-1));
        assertThat(iterator.next(), is(equalTo("Larry")));
        assertThat(iterator.previous(), is(equalTo("Larry")));
        assertThat(iterator.previousIndex(), is(-1));
        try {
            iterator.previous();
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(NoSuchElementException.class)));
        }
        assertThat(iterator.nextIndex(), is(0));
        assertThat(iterator.next(), is(equalTo("Larry")));
        assertThat(iterator.previousIndex(), is(0));
        assertThat(iterator.next(), is(equalTo("Moe")));
        assertThat(iterator.previousIndex(), is(1));
        assertThat(iterator.previous(), is(equalTo("Moe")));

        assertThat(iterator.previousIndex(), is(0));
        assertThat(iterator.next(), is(equalTo("Moe")));
        assertThat(iterator.previousIndex(), is(1));
        assertThat(iterator.next(), is(equalTo("Curly")));
        assertThat(iterator.previousIndex(), is(2));
        assertThat(iterator.previous(), is(equalTo("Curly")));
        assertThat(iterator.previousIndex(), is(1));
        assertThat(iterator.nextIndex(), is(2));
        assertThat(iterator.next(), is(equalTo("Curly")));
        assertThat(iterator.nextIndex(), is(3));
    }

    @Test
    public void remove() throws Exception {
        DoubleLinkedList<String> list = new DoubleLinkedList<>();
        list.addAll(Arrays.asList("Larry", "Moe", "Curly"));
        ListIterator<String> iterator = list.listIterator();
        try {
            iterator.remove();
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(IllegalStateException.class)));
        }
        assertThat(iterator.next(), is(equalTo("Larry")));
        iterator.remove();
        assertThat(list.size(), is(2));
        try {
            iterator.remove();
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(IllegalStateException.class)));
        }
        assertThat(iterator.next(), is(equalTo("Moe")));
        iterator.remove();
        assertThat(list.size(), is(1));
        try {
            iterator.previous();
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(NoSuchElementException.class)));
        }
        assertThat(iterator.next(), is(equalTo("Curly")));

        //remove from medium
        list = new DoubleLinkedList<>();
        list.addAll(Arrays.asList("Larry", "Moe", "Curly"));
        iterator = list.listIterator(1);
        assertThat(iterator.next(), is(equalTo("Moe")));
        iterator.remove();
        assertThat(iterator.previous(), is(equalTo("Larry")));
        assertThat(iterator.next(), is(equalTo("Larry")));
        assertThat(iterator.next(), is(equalTo("Curly")));
        assertThat(list.size(), is(2));
    }

    @Test
    public void set() throws Exception {
        ListIterator<String> iterator = list.listIterator();
        try {
            iterator.set("Tomas");
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(IllegalStateException.class)));
        }
        assertThat(iterator.next(), is(equalTo("Larry")));
        assertThat(iterator.next(), is(equalTo("Moe")));
        iterator.set("Tomas");
        assertThat(iterator.previous(), is(equalTo("Tomas")));
        assertThat(iterator.next(), is(equalTo("Tomas")));
        assertThat(iterator.previousIndex(), is(1));
        assertThat(iterator.previous(), is(equalTo("Tomas")));
        assertThat(iterator.previousIndex(), is(0));
        assertThat(iterator.next(), is(equalTo("Tomas")));
        assertThat(iterator.previousIndex(), is(1));
        assertThat(iterator.nextIndex(), is(2));
        assertThat(iterator.next(), is(equalTo("Curly")));
        assertThat(iterator.nextIndex(), is(3));
    }

    @Test
    public void add() throws Exception {
        DoubleLinkedList<String> list = new DoubleLinkedList<>();
        list.addAll(Arrays.asList("Larry", "Moe", "Curly"));
        ListIterator<String> iterator = list.listIterator();
        assertThat(list.size(), is(3));
        iterator.add("Jhon");
        assertThat(list.size(), is(4));
        iterator.add("Henry");
        iterator.add("Dave");
        assertThat(list.size(), is(6));
        assertThat(iterator.nextIndex(), is(3));
        assertThat(iterator.next(), is(equalTo("Larry")));
        assertThat(iterator.next(), is(equalTo("Moe")));
        iterator.add("Mike");
        assertThat(iterator.previous(), is(equalTo("Mike")));
        assertThat(iterator.next(), is(equalTo("Mike")));
        assertThat(iterator.next(), is(equalTo("Curly")));
        assertThat(list.size(), is(7));

        //With Empty List
        list = new DoubleLinkedList<>();
        iterator = list.listIterator();
        iterator.add("Mike");
        assertThat(iterator.previous(), is(equalTo("Mike")));
        iterator.add("Jhon");
        assertThat(iterator.previous(), is(equalTo("Jhon")));
        iterator.add("Larry");
        assertThat(iterator.previous(), is(equalTo("Larry")));
        iterator.next();
        assertThat(iterator.next(), is(equalTo("Jhon")));
        assertThat(iterator.next(), is(equalTo("Mike")));
    }
}