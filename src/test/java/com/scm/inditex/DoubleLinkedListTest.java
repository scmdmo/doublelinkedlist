package com.scm.inditex;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.*;

/**
 * Created by domin on 28/10/17.
 */
public class DoubleLinkedListTest {

    private DoubleLinkedList<String> list = new DoubleLinkedList<>();

    @Before
    public void setUp() throws Exception {
        list.addAll(Arrays.asList("Larry", "Moe", "Curly", "Jhon", "Peter"));
    }

    @Test
    public void size() throws Exception {
        assertThat(list.size(), is(5));
        List<Integer> list = new DoubleLinkedList<>();
        assertThat(list.size(), is(0));
        list.add(1);
        list.add(2);
        assertThat(list.size(), is(2));
    }

    @Test
    public void isEmpty() throws Exception {
        assertThat(list.isEmpty(), is(false));
        list.clear();
        assertThat(list.isEmpty(), is(true));
        List<Integer> list = new DoubleLinkedList<>();
        assertThat(list.isEmpty(), is(true));
    }

    @Test
    public void contains() throws Exception {
        assertThat(list.contains("Larry"), is(true));
        assertThat(list.contains("Jhon"), is(true));
        assertThat(list.contains("Peter"), is(true));
        assertThat(list.contains("Peter"), is(true));
        assertThat(list.contains("Dave"), is(false));
        assertThat(list.contains("Ben"), is(false));
        list.add("Ben");
        assertThat(list.contains("Ben"), is(true));
    }

    @Test
    public void iterator() throws Exception {
        Iterator<String> iterator = list.iterator();
        assertThat(iterator.next(), is(equalTo("Larry")));
    }

    @Test
    public void toArray() throws Exception {
        Object[] arr = list.toArray();
        assertThat(arr, is(arrayWithSize(list.size())));
        String arr2[] = {"Larry", "Moe", "Curly", "Jhon", "Peter"};
        assertArrayEquals(arr, arr2);
    }

    @Test
    public void toArray1() throws Exception {
        Integer[] intarr = new Integer[5];
        try {
            list.toArray(intarr);
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(ArrayStoreException.class)));
        }
        String[] arrsmall = new String[3];
        assertTrue((list.toArray(arrsmall)) instanceof String[]);
        String[] arr = list.toArray(arrsmall);
        assertThat(arrsmall == arr, is(false));
        assertThat(arr, is(arrayWithSize(list.size())));
        String arr2[] = {"Larry", "Moe", "Curly", "Jhon", "Peter"};
        assertArrayEquals(arr, arr2);
        assertThat(arr, is(arrayContaining(arr2)));

        arrsmall = new String[10];
        arr = list.toArray(arrsmall);
        assertThat(arrsmall == arr, is(true));
        assertThat(arrsmall[5], is(nullValue()));
        assertThat(arrsmall[9], is(nullValue()));
        assertThat(arrsmall, is(arrayContaining("Larry", "Moe", "Curly", "Jhon", "Peter", null, null, null, null, null)));
    }

    @Test
    public void add() throws Exception {
        assertThat(list.contains("Ben"), is(false));
        assertThat(list.add("Ben"), is(true));
        assertThat(list.contains("Ben"), is(true));
        //with empty list
        List<Integer> list = new DoubleLinkedList<>();
        assertThat(list.size(), is(0));
        list.add(1);
        assertThat(list.size(), is(1));
        assertThat(list.get(0), is(1));
    }

    @Test
    public void remove() throws Exception {
        assertThat(list.size(), is(5));
        assertThat(list.contains("Curly"), is(true));
        assertThat(list.remove("Curly"), is(true));
        assertThat(list.contains("Curly"), is(false));
        assertThat(list.size(), is(4));
        assertThat(list.contains("Larry"), is(true));
        assertThat(list.remove("Larry"), is(true));
        assertThat(list.contains("Larry"), is(false));
        assertThat(list.size(), is(3));
        assertThat(list.contains("Peter"), is(true));
        assertThat(list.remove("Peter"), is(true));
        assertThat(list.contains("Peter"), is(false));
        assertThat(list.size(), is(2));
        assertThat(list.remove("Mike"), is(false));
        assertThat(list.size(), is(2));

        //remove deletes the first ocurrence
        list.clear();
        list.addAll(Arrays.asList("Larry", "Moe", "Peter", "Jhon", "Peter"));
        list.remove("Peter");
        assertThat(list, is(org.hamcrest.Matchers.contains("Larry", "Moe", "Jhon", "Peter")));
    }

    @Test
    public void containsAll() throws Exception {
        List<String> res = Arrays.asList("Larry", "Moe", "Curly", "Jhon", "Peter");
        assertThat(list.containsAll(res), is(true));
        res = Arrays.asList("Peter", "Curly", "Jhon", "Peter", "Larry", "Moe", "Jhon");
        assertThat(list.containsAll(res), is(true));
        res = Arrays.asList("Ben", "Moe");
        assertThat(list.containsAll(res), is(false));
    }

    @Test
    public void addAll() throws Exception {
        List<String> res = Arrays.asList("Ben", "Moe");
        assertThat(list.containsAll(res), is(false));
        assertThat(list.addAll(res), is(true));
        assertThat(list.containsAll(res), is(true));
        assertThat(list.size(), is(7));
        assertThat(list.get(list.size() - 2), is("Ben"));
        assertThat(list.get(list.size() - 1), is("Moe"));
        //with empty list
        List<String> list = new DoubleLinkedList<>();
        assertThat(list.size(), is(0));
        list.addAll(res);
        assertThat(list.size(), is(2));
        assertThat(list.get(0), is("Ben"));
    }

    @Test
    public void addAll1() throws Exception {
        List<String> res = Arrays.asList("Ben", "Moe");
        assertThat(list.containsAll(res), is(false));
        assertThat(list.addAll(0, res), is(true));
        assertThat(list.containsAll(res), is(true));
        assertThat(list.size(), is(7));
        assertThat(list.get(0), is("Ben"));
        assertThat(list.get(1), is("Moe"));
        assertThat(list, is(org.hamcrest.Matchers.contains("Ben", "Moe", "Larry", "Moe", "Curly", "Jhon", "Peter")));

        //with empty list
        List<String> list = new DoubleLinkedList<>();
        assertThat(list.size(), is(0));
        list.addAll(0, res);
        assertThat(list.size(), is(2));
        assertThat(list.get(0), is("Ben"));
        assertThat(list, is(org.hamcrest.Matchers.contains("Ben", "Moe")));
        try {
            list.addAll(20, res);
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(IndexOutOfBoundsException.class)));
        }
    }

    @Test
    public void addAll2() throws Exception {
        //Add in the middle
        List<String> res = Arrays.asList("Maggie", "Jenny");
        list.addAll(2, res);
        assertThat(list, is(org.hamcrest.Matchers.contains("Larry", "Moe", "Maggie", "Jenny", "Curly", "Jhon", "Peter")));
        assertThat(list.size(), is(7));

        //Add in the first(empty and no empty)
        list.clear();
        list.addAll(0, res);
        assertThat(list, is(org.hamcrest.Matchers.contains("Maggie", "Jenny")));
        list.addAll(0, res);
        assertThat(list, is(org.hamcrest.Matchers.contains("Maggie", "Jenny", "Maggie", "Jenny")));

        //Add at last
        List<String> res2 = Arrays.asList("Jessy", "Mary");
        list.addAll(4, res2);
        assertThat(list, is(org.hamcrest.Matchers.contains("Maggie", "Jenny", "Maggie", "Jenny", "Jessy", "Mary")));
        assertThat(list.size(), is(6));
    }


    @Test
    public void removeAll() throws Exception {
        //No similar elements
        List<String> res2 = Arrays.asList("Jessy", "Mary", "Maggie", "Jenny");
        assertThat(list.removeAll(res2), is(false));

        //With elements
        List<String> list = new DoubleLinkedList<>();
        List<String> res = Arrays.asList("Maggie", "Jenny", "Moe", "Maggie", "Jenny", "Jessy", "Fred", "Paul", "Mary");
        list.addAll(res);
        assertThat(list.removeAll(res2), is(true));
        assertThat(list, is(org.hamcrest.Matchers.contains("Moe", "Fred", "Paul")));
        assertThat(list.size(), is(3));
    }

    @Test
    public void retainAll() throws Exception {
        //No similar elements
        List<String> res2 = Arrays.asList("Jessy", "Mary", "Maggie", "Jenny");
        assertThat(list.retainAll(res2), is(true));
        assertThat(list.size(), is(0));

        //With elements
        List<String> list = new DoubleLinkedList<>();
        List<String> res = Arrays.asList("Maggie", "Jenny", "Moe", "Maggie", "Jenny", "Jessy", "Fred", "Paul", "Mary");
        list.addAll(res);
        assertThat(list.retainAll(res2), is(true));
        assertThat(list, is(org.hamcrest.Matchers.contains("Maggie", "Jenny", "Maggie", "Jenny", "Jessy", "Mary")));
        assertThat(list.size(), is(6));
    }

    @Test
    public void clear() throws Exception {
        assertThat(list.size(), is(5));
        list.clear();
        assertThat(list.size(), is(0));
        try {
            list.get(0);
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(IndexOutOfBoundsException.class)));
        }
    }

    @Test
    public void get() throws Exception {
        assertThat(list.get(0), is("Larry"));
        assertThat(list.get(1), is("Moe"));
        assertThat(list.get(2), is("Curly"));
        assertThat(list.get(3), is("Jhon"));
        assertThat(list.get(4), is("Peter"));
    }

    @Test
    public void set() throws Exception {
        assertThat(list.get(4), is("Peter"));
        assertThat(list.set(4, "Maggie"), is("Peter"));
        assertThat(list.get(4), is("Maggie"));
        try {
            list.set(7, "Maggie");
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(IndexOutOfBoundsException.class)));
        }
    }

    @Test
    public void add1() throws Exception {
        assertThat(list.contains("Ben"), is(false));
        list.add(0, "Ben");
        assertThat(list.contains("Ben"), is(true));
        assertThat(list.size(), is(6));
        assertThat(list.get(0), is("Ben"));
        assertThat(list, is(org.hamcrest.Matchers.contains("Ben", "Larry", "Moe", "Curly", "Jhon", "Peter")));

        //with empty list
        List<String> list = new DoubleLinkedList<>();
        assertThat(list.size(), is(0));
        list.add(0, "Ben");
        list.add(1, "Moe");
        assertThat(list.size(), is(2));
        assertThat(list.get(0), is("Ben"));
        assertThat(list, is(org.hamcrest.Matchers.contains("Ben", "Moe")));
        try {
            list.add(20, "George");
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(IndexOutOfBoundsException.class)));
        }
    }

    @Test
    public void add2() throws Exception {
        //Add in the middle
        list.add(2, "Maggie");
        list.add(2, "Jenny");
        assertThat(list, is(org.hamcrest.Matchers.contains("Larry", "Moe", "Jenny", "Maggie", "Curly", "Jhon", "Peter")));
        assertThat(list.size(), is(7));

        //Add in the first(empty and no empty)
        list.clear();
        list.add(0, "Maggie");
        list.add(1, "Jenny");
        assertThat(list, is(org.hamcrest.Matchers.contains("Maggie", "Jenny")));
        list.add(0, "Daisy");
        assertThat(list, is(org.hamcrest.Matchers.contains("Daisy", "Maggie", "Jenny")));

        //Add at last
        list.add(3, "Linda");
        assertThat(list, is(org.hamcrest.Matchers.contains("Daisy", "Maggie", "Jenny", "Linda")));
        assertThat(list.size(), is(4));
    }

    @Test
    public void remove1() throws Exception {
        assertThat(list.size(), is(5));
        assertThat(list.get(2), is("Curly"));
        assertThat(list.remove(2), is("Curly"));
        assertThat(list.contains("Curly"), is(false));
        assertThat(list.size(), is(4));

        assertThat(list.get(0), is("Larry"));
        assertThat(list.remove(0), is("Larry"));
        assertThat(list.contains("Larry"), is(false));
        assertThat(list.size(), is(3));

        assertThat(list.get(2), is("Peter"));
        assertThat(list.remove(2), is("Peter"));
        assertThat(list.contains("Peter"), is(false));
        assertThat(list.size(), is(2));

        try {
            list.remove(-1);
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(IndexOutOfBoundsException.class)));
        }
    }

    @Test
    public void indexOf() throws Exception {
        assertThat(list.get(2), is("Curly"));
        assertThat(list.indexOf("Curly"), is(2));
        assertThat(list.get(0), is("Larry"));
        assertThat(list.indexOf("Larry"), is(0));
        assertThat(list.get(4), is("Peter"));
        assertThat(list.indexOf("Peter"), is(4));

        //Repeated elements
        List<String> list = new DoubleLinkedList<>();
        list.addAll(Arrays.asList("Larry", "Moe", "Curly", "Curly", "Larry"));
        assertThat(list.get(0), is("Larry"));
        assertThat(list.indexOf("Larry"), is(0));
        assertThat(list.get(1), is("Moe"));
        assertThat(list.indexOf("Moe"), is(1));
        assertThat(list.get(2), is("Curly"));
        assertThat(list.indexOf("Curly"), is(2));

        //no contain
        assertThat(list.indexOf("Dave"), is(-1));
    }

    @Test
    public void lastIndexOf() throws Exception {
        assertThat(list.get(2), is("Curly"));
        assertThat(list.lastIndexOf("Curly"), is(2));
        assertThat(list.get(0), is("Larry"));
        assertThat(list.lastIndexOf("Larry"), is(0));
        assertThat(list.get(4), is("Peter"));
        assertThat(list.lastIndexOf("Peter"), is(4));

        //Repeated elements
        List<String> list = new DoubleLinkedList<>();
        list.addAll(Arrays.asList("Larry", "Moe", "Curly", "Curly", "Larry"));
        assertThat(list.get(4), is("Larry"));
        assertThat(list.lastIndexOf("Larry"), is(4));
        assertThat(list.get(1), is("Moe"));
        assertThat(list.lastIndexOf("Moe"), is(1));
        assertThat(list.get(3), is("Curly"));
        assertThat(list.lastIndexOf("Curly"), is(3));

        //no contain
        assertThat(list.lastIndexOf("Dave"), is(-1));
    }

    @Test
    public void listIterator() throws Exception {
        try {
            list.listIterator(-1);
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(IndexOutOfBoundsException.class)));
        }
    }

    @Test
    public void subList() throws Exception {
        //Equal list
        DoubleLinkedList<String> subList = list.subList(0, list.size());
        assertThat(subList, is(org.hamcrest.Matchers.contains("Larry", "Moe", "Curly", "Jhon", "Peter")));
        assertThat(subList.size(), is(list.size()));

        //List of one
        subList = list.subList(2, 3);
        assertThat(subList, is(org.hamcrest.Matchers.contains("Curly")));
        assertThat(subList.size(), is(1));

        //SubList
        subList = list.subList(1, 4);
        assertThat(subList, is(org.hamcrest.Matchers.contains("Moe", "Curly", "Jhon")));
        assertThat(subList.size(), is(3));

        subList = list.subList(3, 3);
        assertThat(subList.size(), is(0));

        try {
            list.subList(0, list.size() + 1);
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(IndexOutOfBoundsException.class)));
        }
        try {
            list.subList(3, 2);
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(IndexOutOfBoundsException.class)));
        }
        try {
            list.subList(-5, 0);
            fail();
        } catch (Exception e) {
            assertThat(e, is(instanceOf(IndexOutOfBoundsException.class)));
        }
    }

}