package com.scm.inditex;

import java.util.*;

/**
 * Created by domingo on 26/10/17.
 */
public class DoubleLinkedList<E> implements List<E> {
    private class Node {
        E element;
        Node prev;
        Node next;

        Node(E element, Node prev, Node next) {
            this.element = element;
            this.prev = prev;
            this.next = next;
        }
    }

    private int size = 0;
    private Node first;
    private Node last;

    private Node mediumSearch(Object o) {
        int count = 0;
        Node auxFirst = first;
        Node auxLast = last;
        while (count < size) {
            if (auxFirst.element.equals(o)) {
                return auxFirst;
            }
            if (auxLast.element.equals(o)) {
                return auxLast;
            }
            auxFirst = auxFirst.next;
            auxLast = auxLast.prev;
            count = count + 2;
        }
        return null;
    }

    private Node gotoIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        if (index == 0) {
            return first;
        }
        if (index == size - 1) {
            return last;
        }
        Node aux;
        if (index > size / 2) {
            aux = last;
            int i = size - 1;
            while (i != index) {
                aux = aux.prev;
                i--;
            }
        } else {
            aux = first;
            int i = 0;
            while (i != index) {
                aux = aux.next;
                i++;
            }
        }
        return aux;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return (mediumSearch(o) != null);
    }

    @Override
    public Iterator<E> iterator() {
        return new DoubleLinkedListIterator();
    }

    @Override
    public Object[] toArray() {
        Object[] res = new Object[size];
        int i = 0;
        for (Object o : this) {
            res[i] = o;
            i++;
        }
        return res;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length < size)
            a = (T[]) java.lang.reflect.Array.newInstance(
                    a.getClass().getComponentType(), size);
        int i = 0;
        for (E o : this) {
            a[i] = (T) o;
            i++;
        }
        if (a.length > size)
            a[size] = null;
        return a;
    }

    @Override
    public boolean add(E element) {
        Node node = new Node(element, null, null);
        if (last == null) {
            first = node;
            last = node;
        } else {
            last.next = node;
            node.prev = last;
            last = node;
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        ListIterator<E> listIterator = this.listIterator();
        while (listIterator.hasNext()) {
            if (o.equals(listIterator.next())) {
                listIterator.remove();
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        for (E o : c) {
            add(o);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        if (index == size)
            return addAll(c);
        ListIterator<E> iterator = this.listIterator(index);
        for (E element : c) {
            iterator.add(element);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean res = false;
        for (Object e : c) {
            while (contains(e)) {
                remove(e);
                res = true;
            }
        }
        return res;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean res = false;
        ListIterator<E> listIterator = this.listIterator();
        while (listIterator.hasNext()) {
            if (!c.contains(listIterator.next())) {
                listIterator.remove();
                res = true;
            }
        }
        return res;
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public E get(int index) {
        return gotoIndex(index).element;
    }

    @Override
    public E set(int index, E element) {
        Node n = gotoIndex(index);
        E oldElement = n.element;
        n.element = element;
        return oldElement;
    }

    @Override
    public void add(int index, E element) {
        if (index == size) {
            add(element);
            return;
        }
        Node n = gotoIndex(index);
        Node m = new Node(element, n.prev, n);
        if (n.prev != null) {
            n.prev.next = m;
        } else {
            first = m;
        }
        n.prev = m;
        size++;
    }

    @Override
    public E remove(int index) {
        Node n = gotoIndex(index);
        Node prev = n.prev;
        Node next = n.next;
        if (prev != null) {
            prev.next = next;
        } else {
            first = next;
        }
        if (next != null) {
            next.prev = prev;
        } else {
            last = prev;
        }
        size--;
        return n.element;
    }

    @Override
    public int indexOf(Object o) {
        int i = 0;
        for (E element : this) {
            if (element.equals(o))
                return i;
            i++;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        int i = size - 1;
        ListIterator<E> iterator = new DoubleLinkedListIterator(last, i);
        if (iterator.next().equals(o))
            return i;
        while (iterator.hasPrevious()) {
            if (iterator.previous().equals(o))
                return iterator.nextIndex();
        }
        return -1;
    }

    @Override
    public ListIterator<E> listIterator() {
        return new DoubleLinkedListIterator();
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return new DoubleLinkedListIterator(gotoIndex(index), index);
    }

    @Override
    public DoubleLinkedList<E> subList(int fromIndex, int toIndex) {
        if (fromIndex < 0 || fromIndex >= size || toIndex < 0 || toIndex > size || fromIndex > toIndex)
            throw new IndexOutOfBoundsException();
        DoubleLinkedList<E> subList = new DoubleLinkedList<>();
        if (toIndex == fromIndex)
            return subList;
        Node first = gotoIndex(fromIndex);
        Node last = gotoIndex(toIndex - 1);
        for (Node n = first; n != last.next; n = n.next) {
            subList.add(n.element);
        }
        return subList;
    }

    public class DoubleLinkedListIterator implements ListIterator<E> {
        private int lastReturnIndex;
        private Node cursor;
        private Node prevCursor;
        private Node lastReturn;

        public DoubleLinkedListIterator() {
            cursor = first;
            lastReturnIndex = -1;
        }

        private DoubleLinkedListIterator(Node cursor, int index) {
            this.cursor = cursor;
            this.lastReturnIndex = index - 1;
            if (cursor != null)
                this.prevCursor = cursor.prev;
        }

        @Override
        public boolean hasNext() {
            return (cursor != null);
        }

        @Override
        public E next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            prevCursor = cursor;
            cursor = cursor.next;
            lastReturn = prevCursor;
            lastReturnIndex++;
            return prevCursor.element;
        }

        @Override
        public boolean hasPrevious() {
            return (prevCursor != null);
        }

        @Override
        public E previous() {
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            cursor = prevCursor;
            prevCursor = prevCursor.prev;
            lastReturn = cursor;
            lastReturnIndex--;
            return cursor.element;
        }

        @Override
        public int nextIndex() {
            return lastReturnIndex + 1;
        }

        @Override
        public int previousIndex() {
            return lastReturnIndex;
        }

        @Override
        public void remove() {
            if (lastReturn == null)
                throw new IllegalStateException();
            if (lastReturn == prevCursor) {
                prevCursor = prevCursor.prev;
                lastReturnIndex--;
            } else {  //lastReturn==cursor
                cursor = cursor.next;
            }
            Node prev = prevCursor;
            Node next = cursor;
            if (prev != null) {
                prev.next = next;
            } else {
                first = next;
            }
            if (next != null) {
                next.prev = prev;
            } else {
                last = prev;
            }
            lastReturn = null;
            size--;
        }

        @Override
        public void set(E e) {
            if (lastReturn == null)
                throw new IllegalStateException();
            lastReturn.element = e;
        }

        @Override
        public void add(E e) {
            Node node = new Node(e, prevCursor, cursor);
            if (prevCursor != null) {
                prevCursor.next = node;
            } else {
                first = node;
            }
            if (cursor != null) {
                cursor.prev = node;
            } else {
                last = node;
            }
            prevCursor = node;
            lastReturnIndex++;
            size++;
        }
    }
}