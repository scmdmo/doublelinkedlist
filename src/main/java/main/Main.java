package main;

import com.scm.inditex.DoubleLinkedList;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static <E> void printList(List<E> list){
        System.out.print("Printing List: ");
        for(E e : list ){
            System.out.print(e.toString()+" ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        System.out.println("Filling list with some data");
        DoubleLinkedList<String> list = new DoubleLinkedList<>();
        list.add("Joe");
        list.add("Betty");
        list.addAll(Arrays.asList("Larry", "Moe", "Curly", "Jhon", "Peter"));
        list.add(0,"James");
        list.add(0,"Daisy");
        list.add(4,"Ben");
        list.addAll(5,Arrays.asList("Peter", "Curly", "Jhon", "Peter", "Larry", "Moe", "Jhon"));
        printList(list);

        System.out.println("The indexof and lastIndexof Jhon: "+list.indexOf("Jhon")+" "+list.lastIndexOf("Jhon"));
        System.out.println("Removing, first and last Peter");
        list.remove("Peter");
        list.remove(list.lastIndexOf("Peter"));
        printList(list);

        List<String> res = Arrays.asList("Jhon", "Larry", "Curly", "Jenny", "Moe");
        System.out.println("Remove all "+res);
        list.removeAll(res);
        printList(list);
    }
}